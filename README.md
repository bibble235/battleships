# Battleships

Your challenge is to implement this simplified game of Battleships using text input and output.

The computer randomly chooses the location of two single-cell "ships" on a board of 8 by 8 cells.  The user then has 20 guesses to find the two ships.

The user enters a co-ordinate, for example `3,5`, and the computer locates the nearest ship to that co-ordinate and tells them they're "hot" if they're 1 to 2 cells away, "warm" if they're 3 to 4 cells away, or "cold" if they're further away.

As an example, `3,5` is three cells away from `2,7` because (3 - 2) + (7 - 5) = 3, so they'd be told they were "warm".

If the user correctly guesses a ship's location, they're told they've got a hit and that ship is removed from the board.  The game ends when both ships have been hit by the user, or the user has used up their 20 guesses.

Write your code in a style that you consider to be production quality.

Some things to note:
* We're more interested in your logical thinking, process and coding style.
* Feel free to use your language of choice. We prefer C#, Java, JavaScript, TypeScript, or Python.
* Please create a merge request when you are done.

[![MIT License][license-url]][license-url]

# trademe

## Contents

- [Battleships](#battleships)
- [trademe](#trademe)
  - [Contents](#contents)
  - [Description](#description)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
  - [Testing](#testing)
  - [Run](#run)
  - [Coverage](#coverage)
  - [Acknowledgements](#acknowledgements)
  - [Time Taken](#time-taken)
    - [Breakdown](#breakdown)
    - [Assumptions](#assumptions)
    - [Things I might to differently](#things-i-might-to-differently)
    - [And Finally](#and-finally)
  - [Contact](#contact)
  - [License](#license)

## Description
This is an example battleships program.

<!-- PREREQUISITES -->
## Prerequisites
This example was test using the following set up
- Ubuntu 21.04 (hursute)
- Dotnet 5.0.202

There is nothing specific which would prevent usage on other platforms, however it has only be testing using the above specification

<!-- INSTALLATION -->
## Installation

1.  Clone repository

    * git clone https://github.com/Refactorahau/Battleships-Iain-Wiseman 

<!-- TEST -->
## Testing
```sh
dotnet test
```
## Run
Start using it! 
```sh
cd <project>/src/trademeApp
dotnet run bin/Debug/net5.0/trademeApp
```
<!-- COVERAGE -->
## Coverage
I need to work on this but here is how I do it.
```sh
dotnet test --collect:"XPlat Code Coverage"
# GUID from above command output
reportgenerator "-reports:<project>/src/trademeTest/TestResults/<guid>/coverage.cobertura.xml" "-targetdir:coveragereport" "-reporttypes:Html
```
<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
Kills me to say it but Microsoft Docs with very goodiance-easier/

## Time Taken
### Breakdown
I have probably spent 10 hours on this in all.
- Set up (VS Code with C# on Ubuntu .5 hours
- Understanding Xunit .25 (never used)
- Understanding Coverage .5 (never used - issue with Framework Missing)
- Writing code (~2-3 hours)
- Writing tests (5 hours)
- Documentation README.md (.5 hours)
- Took a while to understand testing of Private Methods and Fields (.5 hours)
### Assumptions
- Whitespace allowed in input
- Duplicate entries allow
### Things I might to differently
Rather depends on the outcome but...
- Format document according to some standards. With C# used to JetBrains
- Think about namespaces
- Look at async with tests
- Automate coverage usage
- Add Tests for Command Interface
### And Finally
- Been 2 years since I have used C# so maybe a little rusty. Time would be greatly reduced with a conversation around what Production means - or not. 
- Hopefully this is not too bad.

<!-- CONTACT -->
## Contact
Iain (Bill) Wiseman - bw@bibble.co.nz

Project Link: [https://github.com/Refactorahau/Battleships-Iain-Wiseman](https://github.com/Refactorahau/Battleships-Iain-Wiseman)

<!-- LICENSE -->
## License
Distributed under the MIT License. See `LICENSE` for more information.

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[license-url]: https://img.shields.io/badge/License-MIT-blue.svg