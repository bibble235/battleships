using System;
using trademe;
using Xunit;

namespace trademeTest
{

    public class GameResponsePlayTest
    {
        [Fact]
        public void TestGameResponsePlayConstructor()
        {

            var gameResponsePlay = new GameResponsePlay(
                1,
                true,
                "Error",
                GameStateEnum.ended,
                "Test Accuracy",
                10);

            Assert.Equal(1, gameResponsePlay.GameContextId);
            Assert.True(gameResponsePlay.Error);
            Assert.Equal("Error", gameResponsePlay.Errormessage);
            Assert.Equal(GameStateEnum.ended, gameResponsePlay.GameState);
            Assert.Equal("Test Accuracy", gameResponsePlay.AccuracyAsText);
            Assert.Equal(10, gameResponsePlay.NumberOfGuessesRemaining);
        }
    }
}