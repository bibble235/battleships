using trademe;
using Xunit;
using System.Reflection;
using System;
using System.Collections.Generic;

namespace trademeTest
{
    public class GameTest
    {
        [Fact]
        public void Play()
        {
            var game = GetDefaultGame();
            var gameResponsePlay = game.Play("xx");
            Assert.True(gameResponsePlay.Error);
            Assert.Equal(gameResponsePlay.NumberOfGuessesRemaining, new GameConfig().NumberOfGuesses);

            game = GetDefaultGame();
            gameResponsePlay = game.Play("1,1");
            Assert.False(gameResponsePlay.Error);
            Assert.Equal(gameResponsePlay.NumberOfGuessesRemaining, new GameConfig().NumberOfGuesses -1);
        }

        [Fact]
        public void TestPlayHit()
        {
            var game = GetDefaultGame();
            var fieldInfo = GetMemberInfo("ships");
            var ships = (Ship[])fieldInfo.GetValue(game);

            var data = $"{ships[0].Location.X},{ships[0].Location.Y}";
            var gameResponsePlay = game.Play(data);
            Assert.False(gameResponsePlay.Error);
            Assert.Equal("Hit", gameResponsePlay.AccuracyAsText);
            Assert.Equal(GameStateEnum.ongoing, gameResponsePlay.GameState);

            data = $"{ships[1].Location.X},{ships[1].Location.Y}";
            gameResponsePlay = game.Play(data);
            Assert.False(gameResponsePlay.Error);
            Assert.Equal("Hit", gameResponsePlay.AccuracyAsText);
            Assert.Equal(GameStateEnum.won, gameResponsePlay.GameState);
        }

        [Fact]
        public void TestPlayEnd()
        {
            GameResponsePlay gameResponsePlay = null;
            var game = GetDefaultGame();
            for(var i = 0 ; i < new GameConfig().NumberOfGuesses -1; i++) {
                gameResponsePlay = game.Play("1,1");
                Assert.NotEqual(GameStateEnum.ended, gameResponsePlay.GameState);
            }

            gameResponsePlay = game.Play("1,1");
            Assert.Equal(GameStateEnum.ended, gameResponsePlay.GameState);
        }

        [Theory]
        [InlineData("8,8,8", "Input vald was not valid. Failed to get Coordinates.")]
        [InlineData("8,7", "Input vald was not valid. X-Coordinate must be between zero and 7.")]
        [InlineData("-1,7", "Input vald was not valid. X-Coordinate must be between zero and 7.")]
        [InlineData("7,-1", "Input vald was not valid. Y-Coordinate must be between zero and 7.")]
        [InlineData("7,8", "Input vald was not valid. Y-Coordinate must be between zero and 7.")]
        [InlineData("xxxxxxxxxxxxxxxxxx", "Input vald was not valid. Failed to get Coordinates.")]
        public void TestCheckDataValidError(string data, string errorMessage)
        {
            var methodInfo = GetMethodInfo("CheckDataValid");
            var error = (string)methodInfo.Invoke(null, new object[]
            {
                new Board(8,8),
                data
            });
            Assert.Equal(errorMessage, error);
        }

        [Fact]
        public void TestCheckDataValid()
        {
            var data = "1,2";
            var methodInfo = GetMethodInfo("CheckDataValid");
            var error = (string)methodInfo.Invoke(null, new object[]
            {
                new Board(8,8),
                data
            });
            Assert.Null(error);
        }

        [Theory]
        [InlineData("8,8", 8, 8)]
        [InlineData("8              , 1", 8, 1)]
        [InlineData("8,                1", 8, 1)]
        [InlineData("            8,                1", 8, 1)]
        public void TestGetCoordinate(string data, int x, int y)
        {
            var methodInfo = GetMethodInfo("GetCoordinate");
            var coordinate = (Coordinate)methodInfo.Invoke(null, new object[]
            {
                data
            });
            Assert.Equal(coordinate.X, x);
            Assert.Equal(coordinate.Y, y);

        }

        [Theory]
        [InlineData(0, 0, "Cold")]
        [InlineData(7, 7, "Hit")]
        [InlineData(7, 2, "Warm")]
        public void TestGetAccuracy(int x, int y, string expectText)
        {
            var coordinate = new Coordinate(x, y);

            var game = GetDefaultGame();

            // Deliberately invert locations.
            var ships = new Ship[] {
                new Ship(new Coordinate(7,7)),
                new Ship(new Coordinate(7,6)),
                new Ship(new Coordinate(7,5))
            };

            var methodInfo = GetMethodInfo("GetAccuracy");
            var accuracyType = (AccuracyTypeEnum)methodInfo.Invoke(null, new object[]
            {
                ships,
                new GameConfig().Accuracies,
                coordinate,
            });

            var methodInfo2 = GetMethodInfo("GetAccuracyAsText");
            var accuracyAsText = (string)methodInfo2.Invoke(null, new object[]
            {
                accuracyType,
            });

            Assert.Equal(expectText, accuracyAsText);
        }

        [Theory]
        [InlineData(0, 0, 14, 13)]
        [InlineData(0, 1, 13, 12)]
        [InlineData(7, 7, 0, 1)]
        [InlineData(6, 7, 1, 0)]
        public void TestGetShipShotDistances(
            int x,
            int y,
            int ship1Distance,
            int ship2Distance)
        {
            var coordinate = new Coordinate(x, y);

            var ships = new Ship[] {
                new Ship(new Coordinate(7,7)),
                new Ship(new Coordinate(6,7))
            };

            var methodInfo = GetMethodInfo("GetShipShotDistances");

            var shipShotDistances = (IList<ShipShotDistance>)methodInfo.Invoke(null, new object[]
            {
                coordinate,
                ships
            });
            Assert.Equal(shipShotDistances[0].Distance, ship1Distance);
            Assert.Equal(shipShotDistances[1].Distance, ship2Distance);
        }

        [Fact]
        public void TestGetShipShotDistancesWithDestroyed()
        {
            var coordinate = new Coordinate(0, 0);

            var ships = new Ship[] {
                new Ship(new Coordinate(7,7)),
                new Ship(new Coordinate(6,7))
            };

            ships[0].ShipState = ShipStateEnum.destroyed;

            var methodInfo = GetMethodInfo("GetShipShotDistances");

            var shipShotDistances = (IList<ShipShotDistance>)methodInfo.Invoke(null, new object[]
            {
                coordinate,
                ships
            });
            Assert.Equal(1, shipShotDistances.Count);
            Assert.Equal(13, shipShotDistances[0].Distance);
        }

        [Fact]
        public void TestGetAccuracyForShipShotDistance()
        {
            var methodInfo = GetMethodInfo("GetAccuracyForShipShotDistance");
            var game = GetDefaultGame();
            var gameConfig = new GameConfig();

            var accuracyTypeEnum = AccuracyTypeEnum.unknown;

            for (var distance = 1; distance < 3; distance++)
            {
                accuracyTypeEnum = (AccuracyTypeEnum)methodInfo.Invoke(null, new object[]
                {
                    gameConfig.Accuracies,distance
                });
                Assert.Equal(AccuracyTypeEnum.hot, accuracyTypeEnum);
            }

            for (var distance = 3; distance < 5; distance++)
            {
                accuracyTypeEnum = (AccuracyTypeEnum)methodInfo.Invoke(null, new object[]
                {
                    gameConfig.Accuracies,distance
                });
                Assert.Equal(AccuracyTypeEnum.warm, accuracyTypeEnum);
            }

            for (var distance = 5; distance < 10; distance++)
            {
                accuracyTypeEnum = (AccuracyTypeEnum)methodInfo.Invoke(null, new object[]
                {
                    gameConfig.Accuracies,distance
                });
                Assert.Equal(AccuracyTypeEnum.cold, accuracyTypeEnum);
            }
        }
        [Fact]
        public void TestShipsInService()
        {
            var game = GetDefaultGame();
            Assert.Equal(2, game.shipsInService);
        }

        [Fact]
        public void TestGetAccuracyAsText()
        {
            var methodInfo = GetMethodInfo("GetAccuracyAsText");
            var game = GetDefaultGame();

            var accuracyAsTextHot = (string)methodInfo.Invoke(null, new object[]
                {
                    AccuracyTypeEnum.hot,
                });

            Assert.Equal("Hot", accuracyAsTextHot);
            var accuracyAsTextWarm = (string)methodInfo.Invoke(null, new object[]
                {
                    AccuracyTypeEnum.warm,
                });

            Assert.Equal("Warm", accuracyAsTextWarm);
            var accuracyAsTextCold = (string)methodInfo.Invoke(null, new object[]
                {
                    AccuracyTypeEnum.cold,
                });
            Assert.Equal("Cold", accuracyAsTextCold);

            var accuracyAsTextHit = (string)methodInfo.Invoke(null, new object[]
                {
                    AccuracyTypeEnum.hit,
                });
            Assert.Equal("Hit", accuracyAsTextHit);

        }

        [Fact]
        public void TestGameCreateResponse()
        {
            var methodInfo = GetMethodInfo("CreateResponse");
            var game = GetDefaultGame();

            var gameResponsePlay = (GameResponsePlay)methodInfo.Invoke(null, new object[]
                {
                    1,
                    GameStateEnum.ongoing,
                    AccuracyTypeEnum.hot,
                    10
                });

            Assert.Equal(1, gameResponsePlay.GameContextId);
            Assert.False(gameResponsePlay.Error);
            Assert.Equal(GameStateEnum.ongoing, gameResponsePlay.GameState);
            Assert.Equal("Hot", gameResponsePlay.AccuracyAsText);
            Assert.Equal(10, gameResponsePlay.NumberOfGuessesRemaining);
        }

        [Fact]
        public void TestGameErrorResponse()
        {
            var methodInfo = GetMethodInfo("CreateErrorResponse");
            var game = GetDefaultGame();

            var gameResponsePlay = (GameResponsePlay)methodInfo.Invoke(null, new object[]
                {
                    1,
                    "We have an Error",
                    10
                });

            Assert.Equal(1, gameResponsePlay.GameContextId);
            Assert.Equal("We have an Error", gameResponsePlay.Errormessage);
            Assert.Equal(10, gameResponsePlay.NumberOfGuessesRemaining);
        }
        static Game GetDefaultGame()
        {
            var gameConfig = new GameConfig();

            var game = new Game(1,
               gameConfig.NumberOfGuesses,
               gameConfig.Board,
               ShipBuilder.Build(
                   gameConfig.Board,
                   gameConfig.NumberOfShips),
               gameConfig.Accuracies);

            return game;
        }

        static MethodInfo GetMethodInfo(string method)
        {
            Type gameType = typeof(Game);

            var methodInfo = gameType.GetMethod(method,
                BindingFlags.NonPublic |
                BindingFlags.Public |
                BindingFlags.Static |
                BindingFlags.FlattenHierarchy);

            return methodInfo;
        }
        static FieldInfo GetMemberInfo(string field)
        {
            Type gameType = typeof(Game);

            var fieldInfo = gameType.GetField(field,
                BindingFlags.NonPublic | BindingFlags.Instance);

            return fieldInfo;
        }
        
    }
}

