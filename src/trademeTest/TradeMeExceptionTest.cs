using trademe;
using Xunit;

namespace trademeTest
{

    public class TradMeExceptionTest
    {
        [Fact]
        public void TestTradMeExceptionConstructor()
        {

            // Test Ok
            var exception = new TradeMeException("Test");
            Assert.Equal("Test", exception.Message);
        }
    }
}