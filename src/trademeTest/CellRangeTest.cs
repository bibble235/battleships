using System;
using trademe;
using Xunit;

namespace trademeTest
{

    public class CellRangeTest
    {
        [Fact]
        public void TestCellRangeConstructor()
        {

            // Test Ok
            var cellRange1 = new CellRange(1, 2);
            Assert.Equal(1, cellRange1.LowerRange);
            Assert.Equal(2, cellRange1.UpperRange);

            // Test lower range = upper range Ok
            var cellRange2 = new CellRange(1, 1);
            Assert.Equal(1, cellRange2.LowerRange);
            Assert.Equal(1, cellRange2.UpperRange);
            
            // Test negative Lower
            Assert.Throws<ArgumentException>(() => new CellRange(-1, 2));
            // Test negative Upper
            Assert.Throws<ArgumentException>(() => new CellRange(1, -1));
            // Test lower range < upper range
            Assert.Throws<ArgumentException>(() => new CellRange(2, 1));
        }

        [Fact]
        public void TestCellRangeEquals()
        {
            var cellRange1 = new CellRange(10, 20);
            var cellRange2 = new CellRange(10, 20);
            var cellRange3 = new CellRange(5, 10);
            CellRange cellRange4 = null;

            Assert.Equal(cellRange1, cellRange2);
            Assert.NotEqual(cellRange1, cellRange3);
            Assert.NotEqual(cellRange3, cellRange4);

        }

    }
}