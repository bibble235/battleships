using trademe;
using Xunit;

namespace trademeTest
{

    public class AccuracyTest
    {
        [Fact]
        public void TestAccuracyConstructor()
        {

            var accuracy1 = new Accuracy(AccuracyTypeEnum.cold, new CellRange(1, 2));
            Assert.Equal(1, accuracy1.CellRange.LowerRange);
            Assert.Equal(2, accuracy1.CellRange.UpperRange);
            Assert.Equal(AccuracyTypeEnum.cold, accuracy1.Type);

            var accuracy2 = new Accuracy(AccuracyTypeEnum.hot, new CellRange(4, 6));
            Assert.Equal(4, accuracy2.CellRange.LowerRange);
            Assert.Equal(6, accuracy2.CellRange.UpperRange);
            Assert.Equal(AccuracyTypeEnum.hot, accuracy2.Type);

            var accuracy3 = new Accuracy(AccuracyTypeEnum.warm, new CellRange(3, 3));
            Assert.Equal(3, accuracy3.CellRange.LowerRange);
            Assert.Equal(3, accuracy3.CellRange.UpperRange);
            Assert.Equal(AccuracyTypeEnum.warm, accuracy3.Type);
        }
    }
}