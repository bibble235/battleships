using System;
using trademe;
using Xunit;

namespace trademeTest
{

    public class CoordinateTest
    {
        [Fact]
        public void TestCoordinateConstructor()
        {

            var coordinate1 = new Coordinate(10, 20);
            Assert.Equal(10, coordinate1.X);
            Assert.Equal(20, coordinate1.Y);

            var coordinate2 = new Coordinate(20, 10);
            Assert.Equal(20, coordinate2.X);
            Assert.Equal(10, coordinate2.Y);

            // Test negative X
            Assert.Throws<ArgumentException>(() => new Coordinate(-1, 2));
            // Test negative Y
            Assert.Throws<ArgumentException>(() => new Coordinate(1, -1));

        }

        [Fact]
        public void TestCoordinateEquals()
        {

            var coordinate1 = new Coordinate(10, 20);
            var coordinate2 = new Coordinate(10, 20);
            var coordinate3 = new Coordinate(20, 10);

            Assert.Equal(coordinate1, coordinate2);
            Assert.NotEqual(coordinate1, coordinate3);
        }

    }
}