using trademe;
using Xunit;

namespace trademeTest
{

    public class ShipTest
    {
        [Fact]
        public void TestShipConstructor()
        {

            var coordinate = new Coordinate(10, 20);
            var ship = new Ship(coordinate);

            Assert.Equal(10, ship.Location.X);
            Assert.Equal(20, ship.Location.Y);
            Assert.Equal(ShipStateEnum.inService, ship.ShipState);
        }

    }
}