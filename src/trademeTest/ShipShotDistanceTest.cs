using System;
using trademe;
using Xunit;

namespace trademeTest
{

    public class ShipShotDistanceTest
    {
        [Fact]
        public void TestShipShotDistanceConstructor()
        {

            var ship = new Ship(new Coordinate(10, 20));

            // Test Positive
            var shipShotDistance = new ShipShotDistance(ship, 100);
            Assert.Equal(new Coordinate(10, 20), shipShotDistance.Ship.Location);
            Assert.Equal(100, shipShotDistance.Distance);

            // Test negative distance
            Assert.Throws<ArgumentException>(() => new ShipShotDistance(ship, -1));

        }
    }
}