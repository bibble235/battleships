using trademe;
using Xunit;

namespace trademeTest
{
    public class ShipBuilderTest
    {
        [Theory]
        [InlineData(8,8)]
        [InlineData(1,3)]
        [InlineData(3,1)]
        public void TestShipBuilderBuild(int width, int height)
        {
            var board = new Board(width, height);
            var ships = ShipBuilder.Build(board, 3);
            Assert.Equal(3, ships.Length);

            // Check the State is ok
            foreach (var ship in ships)
            {
                Assert.Equal(ShipStateEnum.inService, ship.ShipState);
            }

            // Check Locations are all different
            Assert.NotEqual(ships[0].Location, ships[1].Location);
            Assert.NotEqual(ships[1].Location, ships[2].Location);
            Assert.NotEqual(ships[2].Location, ships[0].Location);

            // Check Ships are within Board Bounds
            foreach (var ship in ships)
            {
                Assert.True(ship.Location.X <= board.Width - 1);
                Assert.True(ship.Location.Y <= board.Height - 1);
            }
        }

        [Fact]
        public void TestShipBuilderBuildTooManyShips()
        {
            var board = new Board(1, 1);
            Assert.Throws<TradeMeException>(() => ShipBuilder.Build(board, 2));

            ShipBuilder.Build(board, 1);
        }
    }
}