using trademe;
using Xunit;

namespace trademeTest
{

    public class GameConfigTest
    {
        [Fact]
        public void TestGameConfigConstructor()
        {

            var gameConfig = new GameConfig();
            Assert.Equal(2, gameConfig.NumberOfShips);
            Assert.Equal(20, gameConfig.NumberOfGuesses);
            Assert.Equal(8, gameConfig.Board.Width);
            Assert.Equal(8, gameConfig.Board.Height);
            Assert.Equal(new CellRange(0, 0), gameConfig.Accuracies[0].CellRange);
            Assert.Equal(AccuracyTypeEnum.hit, gameConfig.Accuracies[0].Type);
            Assert.Equal(new CellRange(1, 2), gameConfig.Accuracies[1].CellRange);
            Assert.Equal(AccuracyTypeEnum.hot, gameConfig.Accuracies[1].Type);
            Assert.Equal(new CellRange(3, 4), gameConfig.Accuracies[2].CellRange);
            Assert.Equal(AccuracyTypeEnum.warm, gameConfig.Accuracies[2].Type);
        }

        [Fact]
        public void TestGameConfigSetGuesses()
        {

            var gameConfig = new GameConfig();
            gameConfig.NumberOfGuesses = 50;
            Assert.Equal(50, gameConfig.NumberOfGuesses);
            Assert.Throws<TradeMeException>(() => gameConfig.NumberOfGuesses = 0);
            Assert.Throws<TradeMeException>(() => gameConfig.NumberOfGuesses = -1);
        }

        [Fact]
        public void TestGameConfigSetShips()
        {

            var gameConfig = new GameConfig();
            gameConfig.NumberOfShips = 3;
            Assert.Equal(3, gameConfig.NumberOfShips);
            Assert.Throws<TradeMeException>(() => gameConfig.NumberOfShips = 0);
            Assert.Throws<TradeMeException>(() => gameConfig.NumberOfShips = -1);
        }

        [Fact]
        public void TestGameConfigSetShipsAndGuess()
        {
            var gameConfig = new GameConfig();
            gameConfig.NumberOfShips = 3;
            Assert.Throws<TradeMeException>(() => gameConfig.NumberOfGuesses = 2);
        }

        [Fact]
        public void TestGameConfigSetBoard()
        {
            var gameConfig = new GameConfig();
            gameConfig.Board = new Board(10, 20);
            Assert.Equal(10, gameConfig.Board.Width);
            Assert.Equal(20, gameConfig.Board.Height);
        }

        [Fact]
        public void TestGameConfigSetAccuracies()
        {
            var gameConfig = new GameConfig();
            var accuracy = new Accuracy(AccuracyTypeEnum.cold, new CellRange(10, 20));
            gameConfig.Accuracies = new Accuracy[] { accuracy };
            Assert.Equal(new CellRange(10, 20), gameConfig.Accuracies[0].CellRange);
            Assert.Equal(AccuracyTypeEnum.cold, gameConfig.Accuracies[0].Type);
        }

    }
}