using System;
using trademe;
using Xunit;

namespace trademeTest
{

    public class BoardTest
    {
        [Fact]
        public void TestBoardConstructor()
        {

            // Test Ok
            var board = new Board(1, 2);
            Assert.Equal(1, board.Width);
            Assert.Equal(2, board.Height);

            // Test negative Width
            Assert.Throws<ArgumentException>(() => new Board(-1, 2));
            // Test negative Height
            Assert.Throws<ArgumentException>(() => new Board(1, -1));
        }

        [Fact]
        public void TestBoardSize()
        {

            var board = new Board(10, 20);
            Assert.Equal(10, board.Width);
            Assert.Equal(20, board.Height);
            Assert.Equal(200, board.Size);
        }

    }
}