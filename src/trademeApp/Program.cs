﻿using System;
using trademe;

namespace trademeApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var gameConfig = new GameConfig();

            var game = new Game(
                1,
                gameConfig.NumberOfGuesses,
                gameConfig.Board,
                ShipBuilder.Build(
                    gameConfig.Board,
                    gameConfig.NumberOfShips),
                gameConfig.Accuracies
            );

            var commandLine = new CommandLine(game);
            commandLine.Start();

            Console.WriteLine("Completed Game");
        }
    }
}
