using System;

namespace trademe
{
    public class CommandLine {

        private Game game;
        public CommandLine(Game game) {
            this.game = game;
        }

        public void Start() {
            var keepGoing = true;
            while(keepGoing) {
                var line = Console.ReadLine();
                var playResonse = game.Play(line);
                if(line == "Q") {
                    keepGoing = false;
                }

                // Handle Error
                if(playResonse.Error) {
                    Console.WriteLine(playResonse.Errormessage);
                    continue;
                }

                // Handle Hit
                if(playResonse.AccuracyAsText == "Hit") {
                    Console.WriteLine("Ship Destroyed - boom");
                }
                else {
                    Console.WriteLine($"You were - {playResonse.AccuracyAsText}");
                }

                // Handle No more Guesses
                if(playResonse.GameState ==  GameStateEnum.ended) {
                    Console.WriteLine($"Lost - No more guesses left");
                    keepGoing = false;
                }

                // Handle Won
                if(playResonse.GameState ==  GameStateEnum.won) {
                    Console.WriteLine($"Won - Well done");
                    keepGoing = false;
                }

             }
        }
    }
}