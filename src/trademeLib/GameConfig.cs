using System;

namespace trademe
{
    public class GameConfig {
        private int _numberOfShips;
        private int _numberOfGuesses;
        public Accuracy[] Accuracies {get; set;}
        public int NumberOfShips { 
            get => _numberOfShips; 
            set {
                if(value <= 0) throw new TradeMeException("Number Of Ships must a positive integer");
                _numberOfShips = value;
            }
        }
        public int NumberOfGuesses {
            get => _numberOfGuesses; 
            set {
                if(value <= 0) throw new TradeMeException("Number Of Guesses must a positive integer");
                if(value < NumberOfShips) throw new TradeMeException("Number Of Guesses must greater than or equal to Number Of Ships");
                _numberOfGuesses = value;
            }
        }
        public Board Board { get; set;}
        public GameConfig() {
            this.NumberOfShips = 2;
            this.NumberOfGuesses = 20;
            this.Board = new Board(8,8);
            this.Accuracies = new Accuracy[] {
                new Accuracy(AccuracyTypeEnum.hit, new CellRange(0,0)),
                new Accuracy(AccuracyTypeEnum.hot, new CellRange(1,2)),
                new Accuracy(AccuracyTypeEnum.warm, new CellRange(3,4))
            };
        }
    }
}