using System;

namespace trademe {

    public class TradeMeException : Exception {
        public TradeMeException(string error): base(error) {
        }
    }
}
