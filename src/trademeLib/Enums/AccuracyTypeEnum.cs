namespace trademe
{
    public enum AccuracyTypeEnum
    {
        unknown,
        hit,
        hot,
        warm,
        cold,
    };
}