namespace trademe
{
    public enum GameStateEnum
    {
        ongoing,
        won,
        ended,
    };
}