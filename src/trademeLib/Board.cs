using System;

namespace trademe
{
    public class Board {

        public int Width { get; init;}
        public int Height { get; init;}
        public int Size => Width * Height;

        public Board(int width, int height) {

            if(width < 0) throw new ArgumentException("width must be a positive integer");
            if(height < 0) throw new ArgumentException("height must be a positive integer");

            this.Width = width;
            this.Height = height;
        }

        
    }
}
