using System;

namespace trademe
{
    public class Coordinate {
        public int X { get; init;}
        public int Y { get; init;}
        public Coordinate(int x, int y) {

            if(x < 0) throw new ArgumentException("x must be a positive integer");
            if(y < 0) throw new ArgumentException("y must be a positive integer");

            this.X = x;
            this.Y = y;
        }

        public override bool Equals(Object obj) {
            Coordinate coordinateObj = obj as Coordinate;
            if (coordinateObj == null) {
                return false;
            }
            else {
                return X == coordinateObj.X && Y == coordinateObj.Y;
            }
        }

        public override int GetHashCode() {
            return this.X.GetHashCode() + this.Y.GetHashCode();
        }
    }
}