namespace trademe
{
    public class Accuracy
    {
        public AccuracyTypeEnum Type { get; init; }
        public CellRange CellRange { get; init; }

        public Accuracy(AccuracyTypeEnum type, CellRange cellRange)
        {
            this.Type = type;
            this.CellRange = cellRange;
        }
    }
}