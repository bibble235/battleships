using System;

namespace trademe
{
    public class ShipShotDistance
    {

        public Ship Ship { get; init; }
        public int Distance { get; init; }

        public ShipShotDistance(Ship ship, int distance)
        {

            if (distance < 0) throw new ArgumentException("Distance must be a positive integer");
            this.Ship = ship;
            this.Distance = distance;
        }
    }
}