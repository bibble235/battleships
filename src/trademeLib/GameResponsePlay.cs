namespace trademe
{
    public class GameResponsePlay {
        public int GameContextId {get; init;}
        public bool Error  {get; init;}
        public string Errormessage  {get; init;}
        public GameStateEnum GameState {get; init;}
        public string AccuracyAsText {get; init;}
        public int NumberOfGuessesRemaining {get; init;}
        public GameResponsePlay(
            int gameContextId,
            bool error,
            string errormessage,
            GameStateEnum gameState,
            string accuracyAsText,
            int numberOfGuessesRemaining) {

            this.GameContextId = gameContextId;
            this.Error = error;
            this.Errormessage = errormessage;
            this.GameState = gameState;
            this.AccuracyAsText = accuracyAsText;
            this.NumberOfGuessesRemaining = numberOfGuessesRemaining;
        }
    }
}
