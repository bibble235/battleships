using System;

namespace trademe
{
    public class Ship {
        public Coordinate Location { get; init;}
        public ShipStateEnum ShipState { get; set; }
        public Ship(Coordinate location) {
            this.Location = location;
            this.ShipState = ShipStateEnum.inService;
        }
    }
}