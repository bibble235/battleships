using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

namespace trademe
{
    public class Game
    {
        private readonly int contextId;
        private readonly Board board;
        private readonly Accuracy[] accuracies;
        private readonly int numberOfGuesses;
        private int numberOfGuessesRemaining;
        private Ship[] ships;
        public int shipsInService => ships.Count(ship => ship.ShipState == ShipStateEnum.inService);

        public Game(
            int contextId,
            int numberOfGuesses,
            Board board,
            Ship[] ships,
            Accuracy[] accuracies)
        {

            this.contextId = contextId;
            this.numberOfGuesses = numberOfGuesses;
            this.board = board;
            this.ships = ships;
            this.accuracies = accuracies;
            this.numberOfGuessesRemaining = numberOfGuesses;
        }
        public GameResponsePlay Play(string data)
        {
            // Validate the Data
            var error = CheckDataValid(board, data);

            // Return Error
            if (error != null)
                return CreateErrorResponse(
                    contextId,
                    error,
                    numberOfGuessesRemaining);

            // Extract the Co-ordinate
            var coordinate = GetCoordinate(data);

            // Get Accuracy
            var accuracyType = GetAccuracy(
                    ships,
                    accuracies,
                    coordinate);

            // If a Hit Update the Ship to be destroyed
            if (accuracyType == AccuracyTypeEnum.hit)
            {
                // Update the ship to be destroyed
                var ship = ships.Where(ship => ship.Location.Equals(coordinate)).First();
                Debug.Assert(ship != null);
                if (ship != null)
                {
                    ship.ShipState = ShipStateEnum.destroyed;
                }
            }

            numberOfGuessesRemaining--;

            // Check for End
            // 1. All Ships Destroyed
            // 2. Ran out of Guesses
            var gameState = GameStateEnum.ongoing;

            if (shipsInService == 0)
            {
                gameState = GameStateEnum.won;
            }
            else if (numberOfGuessesRemaining == 0)
            {
                gameState = GameStateEnum.ended;
            }

            // Create Play Response
            return CreateResponse(
                contextId,
                gameState,
                accuracyType,
                numberOfGuessesRemaining);
        }

        static private string CheckDataValid(
            Board board,
            string data)
        {
            string result = null;

            var defaultError = "Input vald was not valid.";

            try
            {
                string[] coordinates = data.Split(',');
                if (coordinates.Length != 2)
                {
                    result = defaultError + " Failed to get Coordinates.";
                    return result;
                }

                var x = int.Parse(coordinates[0]);
                if (x < 0 || x > board.Width - 1)
                {
                    result = defaultError + $" X-Coordinate must be between zero and {board.Width - 1}.";
                    return result;
                }

                var y = int.Parse(coordinates[1]);
                if (y < 0 || y > board.Height - 1)
                {
                    result = defaultError + $" Y-Coordinate must be between zero and {board.Height - 1}.";
                    return result;
                }

                Point point = new Point(
                    x,
                    y);
            }
            catch (Exception)
            {
                // Console.WriteLine($"Exception parsing Coordinate - {exception}");
                result = defaultError + " Failed to get Coordinates. Format is X,Y";
            }

            return result;
        }

        private static Coordinate GetCoordinate(string data)
        {
            string[] coordinates = data.Split(',');
            var x = int.Parse(coordinates[0]);
            var y = int.Parse(coordinates[1]);
            return new Coordinate(x, y);
        }

        static private AccuracyTypeEnum GetAccuracy(
            Ship[] ships,
            Accuracy[] accuracies,
            Coordinate coordinate)
        {

            var shipShotDistances = GetShipShotDistances(coordinate, ships);
            Debug.Assert(shipShotDistances.ToList().Count != 0);

            var nearestShipShotDistance = shipShotDistances.Min(x => x.Distance);

            return GetAccuracyForShipShotDistance(
                accuracies,
                nearestShipShotDistance);
        }

        static private IList<ShipShotDistance> GetShipShotDistances(
            Coordinate coordinate,
            Ship[] ships)
        {

            var availableShips = ships.Where(ship => ship.ShipState == ShipStateEnum.inService);
            Debug.Assert(availableShips.ToList().Count != 0);

            var shipShotDistances = new List<ShipShotDistance>();
            foreach (var ship in availableShips)
            {
                var distance = (Math.Abs(coordinate.X - ship.Location.X)) + (Math.Abs(coordinate.Y - ship.Location.Y));

                // Console.WriteLine($"================================");
                // Console.WriteLine($"Current Location {coordinate.x}, {coordinate.y}");
                // Console.WriteLine($"Ship Location {ship.Location.x}, {ship.Location.y}");
                // Console.WriteLine($"Distance {distance}");
                shipShotDistances.Add(new ShipShotDistance(ship, distance));
            }

            return shipShotDistances;
        }

        static private AccuracyTypeEnum GetAccuracyForShipShotDistance(
            Accuracy[] accuracies,
            int distance)
        {
            foreach (var accuracy in accuracies)
            {
                if (distance >= accuracy.CellRange.LowerRange &&
                   distance <= accuracy.CellRange.UpperRange)
                {
                    return accuracy.Type;
                }
            }
            return AccuracyTypeEnum.cold;
        }

        static private GameResponsePlay CreateErrorResponse(
            int contextId,
            string error,
            int numberOfGuessesRemaining)
        {
            return new GameResponsePlay(
                contextId,
                true,
                error,
                GameStateEnum.ongoing,
                null,
                numberOfGuessesRemaining);
        }

        static private string GetAccuracyAsText(AccuracyTypeEnum accuracy)
        {
            return accuracy switch
            {
                AccuracyTypeEnum.hit => "Hit",
                AccuracyTypeEnum.hot => "Hot",
                AccuracyTypeEnum.warm => "Warm",
                _ => "Cold"
            };
        }
        static private GameResponsePlay CreateResponse(
            int contextId,
            GameStateEnum gameState,
            AccuracyTypeEnum accuracyType,
            int numberOfGuessesRemaining)
        {

            return new GameResponsePlay(
               contextId,
               false,
               null,
               gameState,
               GetAccuracyAsText(accuracyType),
               numberOfGuessesRemaining);
        }
    }
}