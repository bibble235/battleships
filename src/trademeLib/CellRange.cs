using System;

namespace trademe
{
    public class CellRange
    {
        public int LowerRange { get; init; }
        public int UpperRange { get; init; }
        public CellRange(int lowerRange, int upperRange)
        {
            if (lowerRange < 0) throw new ArgumentException("Lower Range must be a positive integer");
            if (upperRange < 0) throw new ArgumentException("Upper Range must be a positive integer");
            if (lowerRange > upperRange) throw new ArgumentException("Lower Range must less than or equal to Upper Range");
            this.LowerRange = lowerRange;
            this.UpperRange = upperRange;
        }

        public override bool Equals(Object obj)
        {
            CellRange cellRangeObj = obj as CellRange;
            if (cellRangeObj == null)
            {
                return false;
            }
            else
            {
                return LowerRange == cellRangeObj.LowerRange && UpperRange == cellRangeObj.UpperRange;
            }
        }

        public override int GetHashCode()
        {
            return this.LowerRange.GetHashCode() + this.UpperRange.GetHashCode();
        }

    }
}