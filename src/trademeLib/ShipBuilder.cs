using System;
using System.Diagnostics;
using System.Linq;

namespace trademe
{
    static public class ShipBuilder
    {
        static public Ship[] Build(            
            Board board,
            int numberOfShips) {

            var locations = CreateShipLocations(
                board,
                numberOfShips);
            
            var ships = new Ship[numberOfShips];
            for(var index = 0; index < numberOfShips; index++) {
                ships[index] = new Ship(locations[index]);
                // Console.WriteLine($"Create Ship at {locations[index].x}, {locations[index].y}");
            }
            return ships;
        }

        static private Coordinate[] CreateShipLocations(
            Board board,
            int numberOfShips)
        {
            if (board.Size < numberOfShips)
            {
                throw new TradeMeException($"Game Configuration Error. Number of Locations ({board.Size}) cannot be less than number of ships ({numberOfShips})");
            }

            int[] allCoordinates =
                Enumerable.Range(0, (board.Size))
                  .Select(i => i)
                  .ToArray();

            // Ramdomize the co-ordinates
            Debug.Assert(allCoordinates.Length == board.Size);
            Debug.Assert(allCoordinates.Length >= numberOfShips);

            Random rnd = new Random();

            int[] allCoordinatesRandomized =
                allCoordinates.OrderBy(x => rnd.Next()).ToArray();

            var shipLocations = new Coordinate[numberOfShips];

            for (var i = 0; i < numberOfShips; i++)
            {
                shipLocations[i] = ConvertLocationToCoordinate(
                    allCoordinatesRandomized[i],
                    board.Height);
            }

            return shipLocations;
        }
        private static Coordinate ConvertLocationToCoordinate(
            int location,
            int numberOfCellsHeight)
        {

            var x = (int)(location / numberOfCellsHeight);
            var y = location - (x * numberOfCellsHeight);

            return new Coordinate(x, y);
        }
    }
}